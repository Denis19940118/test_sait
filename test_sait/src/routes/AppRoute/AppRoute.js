import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from 'react-router';
import Polices from '../../pages/Polices/Polices';
import UserAgreement from '../../pages/UserAgreement/UserAgreement';
import Home from '../../pages/Home/Home';
import Registration from '../../pages/Registration/Registration';
import Thanks from '../../pages/Thanks/Thanks';

const AppRoute = () => {
	return (
		<section>
			<Switch>
				<Redirect exact path="/" to="/home" />
				<Route exact path="/home">
					<Home />
				</Route>
				<Route exact path="/registration">
					<Registration />
				</Route>
				<Route exact path="/thanks">
					<Thanks />
				</Route>
				<Route exact path="/polices">
					<Polices />
				</Route>
				<Route exact path="/agreement">
					<UserAgreement />
				</Route>
			</Switch>
		</section>
	);
};

export default AppRoute;
