import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faUserCircle } from '@fortawesome/free-solid-svg-icons';
import Modal from '../Modal/Modal';
import Button from '../Button/Button';
import { Link } from 'react-router-dom';
import MyForm from '../MyForm/MyForm';
import MyInput from '../MyInput/MyInput';
import BrandBar from '../../components/BrandBar/BrandBar';
import styles from './ModalContainer.module.scss';

const ModalContainer = ({ title, children, goToTheFuture }) => {
	const [opened, setOpened] = useState(true);
	return (
		<>
			{opened && (
				<Modal
					title={title}
					opened={opened}
					setOpened={setOpened}
					children={children}
					childrenOut={<BrandBar width="60%" />}
				/>
			)}
		</>
	);
};

ModalContainer.propTypes = {};

export default ModalContainer;
