import React from 'react';
import PropTypes from 'prop-types';
import styles from './Footer.module.scss';

const Footer = ({ dark, children, color }) => {
	const style = {
		color: color,
	};
	return (
		<footer className={styles.container__footer}>
			{dark && <div className={styles.footer__dark}></div>}
			<div className={styles.container}>{children}</div>
			<div style={style} className={styles.copyright}>
				Copyright © 2021 – Общее дело
			</div>
		</footer>
	);
};

Footer.propTypes = {
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
		.isRequired,
	dark: PropTypes.bool.isRequired,
	color: PropTypes.string.isRequired,
};

export default Footer;
