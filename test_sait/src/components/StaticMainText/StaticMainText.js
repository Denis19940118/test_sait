import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './StaticMainText.module.scss';

const StaticMainText = () => {
	const [up, setUp] = useState(100);
	const [down, setDown] = useState(35);

	const timerUp = () => {
		setTimeout(() => {
			setUp(up + 1);
		}, 5000);
	};
	timerUp();
	const timerDown = () => {
		setTimeout(() => {
			if (down !== 0) {
				setDown(down - 1);
			} else {
				setDown(0);
			}
		}, 5000);
	};
	timerDown();
	return (
		<div className={styles.main_stat}>
			<div>
				<div className={`${styles.main_stat_block} ${styles.online}`}>
					<div className={styles.num}>{up}</div>
					<div className={styles.text}>
						<span> смотрят</span>" страницу "
					</div>
				</div>
				<div className={`${styles.main_stat_block} ${styles.lasts}`}>
					<div className={styles.num}>{down}</div>
					<div className={styles.text}>
						" осталось "<span> мест</span>
					</div>
				</div>
			</div>
		</div>
	);
};

StaticMainText.propTypes = {};

export default StaticMainText;
