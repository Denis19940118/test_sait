import React from 'react';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import styles from './MyForm.module.scss';
import schema from './schema';

const MyForm = props => {
	return (
		<div className={styles.container_form}>
			{!props.modal && (
				<form
					className={styles.form_box}
					onSubmit={props.handleSubmit}
					noValidate
				>
					{props.children}
				</form>
			)}
			{props.modal && (
				<form
					className={styles.form_box_modal}
					onSubmit={() => props.handleSubmit()}
					noValidate
				>
					{props.children}
				</form>
			)}
		</div>
	);
};

MyForm.propTypes = {};

const register = (values, { props, setSubmitting }) => {
	props.goToTheFuture();
	console.log(props);
	console.log(values);

	setSubmitting(false);
};

export default withFormik({
	mapPropsToValues: props => ({
		name: '',
		email: '',
	}),
	handleSubmit: register,
	validationSchema: schema,
})(MyForm);
