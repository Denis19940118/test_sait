import * as yup from 'yup';

const FIELD_REQUIRED = 'This field is required';

const schema = yup.object().shape({
	email: yup
		.string('Enter your email')
		.email('Enter a valid email')
		.required('Email is required'),
	name: yup.string('Enter your name').required(FIELD_REQUIRED),
	lastName: yup.string('Enter your last name').required(FIELD_REQUIRED),
	tel: yup.string('Enter your tel').required(FIELD_REQUIRED),
});

export default schema;
