import React from 'react';
import PropTypes from 'prop-types';
import { withFormik } from 'formik';
import styles from './MyForm.module.scss';
import schemaReg from './schemaReg';

const MyForm = props => {
	const style = {
		background: props.backgroundColor,
		padding: props.padd,
	};

	return (
		<div className={styles.container_form}>
			{!props.modal && (
				<form
					className={styles.form_box}
					onSubmit={props.handleSubmit}
					noValidate
				>
					{props.children}
				</form>
			)}
			{props.modal && (
				<form
					style={style}
					className={styles.form_box_modal}
					onSubmit={props.handleSubmit}
					noValidate
				>
					{props.children}
				</form>
			)}
		</div>
	);
};

MyForm.propTypes = {};

const register = (values, { props, setSubmitting }) => {
	console.log(values);
	props.sendEmail(values);
	props.goToBack();
	setSubmitting(false);
};

export default withFormik({
	mapPropsToValues: props => ({
		name: '',
		lastName: '',
		email: '',
		tel: '',
		sent: props.sent,
	}),
	handleSubmit: register,
	validationSchema: schemaReg,
})(MyForm);
