import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import styles from './Modal.module.scss';

const Modal = ({ opened, setOpened, children, childrenOut, title }) => {
	return (
		<div className={styles.container__modal} onClick={() => setOpened(!opened)}>
			<div
				className={styles.container__modal_form}
				onClick={e => e.stopPropagation()}
			>
				<div className={styles.modal__form} onClick={e => e.stopPropagation()}>
					<div className={styles.modal__form_btn}>
						<Button
							type="button"
							title="x"
							className={styles.modal__btn}
							onClick={() => setOpened(!opened)}
						/>
					</div>
					<h6 className={styles.modal_title}>{title}</h6>
					<div>{children}</div>
					<div>{childrenOut}</div>
				</div>
			</div>
		</div>
	);
};

Modal.propTypes = {
	opened: PropTypes.bool.isRequired,
	setOpened: PropTypes.func.isRequired,
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
		.isRequired,
	childrenOut: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
		.isRequired,
	title: PropTypes.string.isRequired,
};

export default Modal;
