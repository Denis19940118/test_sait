import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './Head.module.scss';

const Head = () => {
	return (
		<div className={styles.container__logo}>
			<Link to="/">
				<picture>
					<img
						className={styles.container__logo}
						src="https://twowords.info/img/main-logo.png"
					/>
				</picture>
			</Link>
		</div>
	);
};

export default Head;
