import React from 'react';
import styles from './Steps.module.scss';

const Steps = () => {
	return (
		<div className={styles.container__steps}>
			<div className={styles.container}>
				<ul className={styles.container__steps_row}>
					<li className={styles.container__steps_col}>Пройдите регистрацию</li>
					<li className={`${styles.container__steps_col} ${styles.active}`}>
						Создайте аккаунт
					</li>
					<li className={styles.container__steps_col}>
						Получите доступ к программе
					</li>
					<li className={styles.container__steps_col}>Вы в деле</li>
				</ul>
			</div>
		</div>
	);
};

export default Steps;
