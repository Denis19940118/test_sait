import React from 'react';
import PropTypes from 'prop-types';
import girl from '../../theme/image/girl.jpg';
import man from '../../theme/image/man.jpg';
import styles from './Slider.module.scss';
const Slider = () => {
	return (
		<div className={styles.done_block__reviews_items}>
			<ul className={styles.review}>
				<li className={styles.review_items}>
					<div className={styles.review__user_photo}>
						<img className={styles.imgs} src={girl} alt="girl" />
					</div>
					<div className={styles.review__info}>
						<div className={styles.review__info_name}>Настя</div>
						<div className={styles.review__info_signature}>
							'только что заработал (а)'
						</div>
						<div className={styles.review__info_money}> $ 505</div>
					</div>
				</li>
				<li className={styles.review_items}>
					<div className={styles.review__user_photo}>
						<img className={styles.imgs} src={man} alt="man" />
					</div>
					<div className={styles.review__info}>
						<div className={styles.review__info_name}>Богдан</div>
						<div className={styles.review__info_signature}>
							'только что заработал (а)'
						</div>
						<div className={styles.review__info_money}> $ 436</div>
					</div>
				</li>
			</ul>
		</div>
	);
};

Slider.propTypes = {};

export default Slider;
