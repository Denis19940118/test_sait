import React from 'react';
import PropTypes from 'prop-types';
import './PageHeading.module.scss';

const PageHeading = ({ className, title }) => {
	return (
		<div className="container__heading">
			<h1 className={className}>{title}</h1>
		</div>
	);
};

PageHeading.propTypes = {
	className: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
};

export default PageHeading;
