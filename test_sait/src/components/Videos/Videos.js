import React, { useState } from 'react';
import PropTypes from 'prop-types';
import videoHome from '../../theme/video/bandicam1.mp4';
import styles from './Videos.module.scss';

const Videos = ({ FontAwesomeIcon, faVolumeUp }) => {
	const [controls] = useState(true);
	const [muted, setMuted] = useState(true);
	const closeIconVolume = () => {
		setMuted(!muted);
	};
	return (
		<div className={styles.video__container}>
			{muted && (
				<div className={styles.icon__volume} onClick={() => closeIconVolume()}>
					<FontAwesomeIcon
						className={styles.icon__volume_item}
						icon={faVolumeUp}
					/>
					<p>Включить звук</p>
				</div>
			)}
			<video
				className={styles.video__item}
				src={videoHome}
				id="video"
				width="620"
				height="370"
				autoPlay="autoplay"
				muted={muted}
				controls={controls}
			>
				Your browser does not support the video tag.
			</video>
		</div>
	);
};

Videos.propTypes = {};

export default Videos;
