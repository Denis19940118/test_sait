import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import styles from './MyInput.module.scss';

const MyInput = ({
	name,
	icon,
	type,
	className,
	placeholder,
	width,
	widthBox,
	backgroundColor,
	...rest
}) => {
	const [field, meta /* helpers */] = useField(name);
	const styleBoxInput = {
		minWidth: widthBox,
	};
	const styleInput = {
		maxWidth: width,
		backgroundColor: backgroundColor,
	};
	return (
		<div className={styles.input_item} style={styleBoxInput}>
			<label className={styles.input_box}>
				<span className={styles.icon}>{icon}</span>
				<input
					className={styles.input}
					type={type}
					placeholder={placeholder}
					style={styleInput}
					{...field}
					{...rest}
				/>
				{meta.error && meta.touched && (
					<span className={styles.error}>{meta.error}</span>
				)}
			</label>
		</div>
	);
};

MyInput.propTypes = {
	icon: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
	type: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,
};

MyInput.propTypes = {
	className: styles.input,
};

export default MyInput;
