import React from 'react';
import PropTypes from 'prop-types';
import imgOne from '../../theme/image/main-footer-item-one.png';
import imgOneFoot from '../../theme/image/reg-footer-item-one.png';
import imgTwo from '../../theme/image/main-footer-item-two.png';
import imgTwoFoot from '../../theme/image/reg-footer-item-two.png';
import imgThree from '../../theme/image/main-footer-item-three.png';
import imgFour from '../../theme/image/main-footer-item-four.png';
import imgFourFoot from '../../theme/image/reg-footer-item-four.png';
import imgFive from '../../theme/image/main-footer-item-five.png';
import styles from './BrandBar.module.scss';

const BrandBar = ({ width, foot }) => {
	const style = {
		width: width,
	};
	const children = () => {
		return (
			<>
				<li>
					<picture>
						{foot && (
							<img className={styles.img_item} src={imgOneFoot} style={style} />
						)}
						{!foot && (
							<img className={styles.img_item} src={imgOne} style={style} />
						)}
					</picture>
				</li>
				<li>
					<picture>
						{foot && (
							<img className={styles.img_item} src={imgTwoFoot} style={style} />
						)}
						{!foot && (
							<img className={styles.img_item} src={imgTwo} style={style} />
						)}
					</picture>
				</li>
				<li>
					<picture>
						<img className={styles.img_item} src={imgThree} style={style} />
					</picture>
				</li>
				<li>
					<picture>
						{foot && (
							<img
								className={styles.img_item}
								src={imgFourFoot}
								style={style}
							/>
						)}
						{!foot && (
							<img className={styles.img_item} src={imgFour} style={style} />
						)}
					</picture>
				</li>
				<li>
					<picture>
						<img className={styles.img_item} src={imgFive} style={style} />
					</picture>
				</li>
			</>
		);
	};

	return (
		<section className={styles.brand__bar}>
			<ul className={styles.img__list}>{children()}</ul>
		</section>
	);
};

BrandBar.propTypes = {};

export default BrandBar;
