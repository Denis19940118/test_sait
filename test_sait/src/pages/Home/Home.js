import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faEnvelope,
	faUserCircle,
	faVolumeUp,
} from '@fortawesome/free-solid-svg-icons';
import { Link, useHistory } from 'react-router-dom';
import Container from '../../components/Containers/Container';
import Button from '../../components/Button/Button';
import MyInput from '../../components/MyInput/MyInput';
import styles from './Home.module.scss';
import MyForm from '../../components/MyForm/MyForm';
import Footer from '../../components/Footer/Footer';
import BrandBar from '../../components/BrandBar/BrandBar';
import Videos from '../../components/Videos/Videos';
import Head from '../../components/Head/Head';
import ModalContainer from '../../components/ModalContainer/ModalContainer';
import StaticMainText from '../../components/StaticMainText/StaticMainText';

const Home = props => {
	const [dark] = useState(true);
	const history = useHistory();
	const goToTheFuture = () => {
		history.push('/registration');
	};

	return (
		<Container>
			<section className={styles.container__home}>
				<Head />
				<StaticMainText />

				<h1 className={styles.heading}>
					Уникальное новшество от программистов, благодаря которому обычные
					русские работяги получают дополнительно от 60.000 рублей в месяц,
					уделяя всего 15 минут в день! Посмотрите видео, пройдите бесплатную
					регистрацию и получите свою первую выплату уже завтра.
				</h1>
				<Videos FontAwesomeIcon={FontAwesomeIcon} faVolumeUp={faVolumeUp} />
				<MyForm
					goToTheFuture={goToTheFuture}
					children={
						<>
							<div className={styles.container__input}>
								<MyInput
									type="text"
									name="name"
									width="100%"
									widthBox="50%"
									icon={
										<>
											<FontAwesomeIcon icon={faUserCircle} />{' '}
										</>
									}
									placeholder="Ваше имя"
								/>
								<MyInput
									type="email"
									name="email"
									width="100%"
									widthBox="50%"
									icon={
										<>
											<FontAwesomeIcon icon={faEnvelope} />{' '}
										</>
									}
									placeholder="Ваш актуальный e-mail"
								/>
							</div>
							<div className={styles.container__btn}>
								<div className={styles.btn_home_box}>
									<Button
										// className={styles.btn_home}
										type="submit"
										title="Пройти регестрацию бесплатно!"
										width="100%"
										padd="0 75"
									/>
								</div>

								<div className={styles.checkbox_license}>
									<input type="checkbox" checked readOnly />
									<span className={styles.text_checkbox}>
										Я согласен на обработку персональных данных и получение
										рекламных материалов, и я согласен с
										<Link className={styles.link_checkbox} to="/polices">
											публичной офертой
										</Link>
									</span>
								</div>
							</div>
						</>
					}
				/>
				<BrandBar />
				<ModalContainer
					title="Начните получать от 2500 у.е. в месяц!"
					goToTheFuture={goToTheFuture}
					children={
						<>
							<MyForm
								goToTheFuture={goToTheFuture}
								modal={true}
								children={
									<>
										<div className={styles.form_title_modal}>
											Пройдите регистрацию и получите бесплатный доступ к
											программе!
										</div>
										<div className={styles.container__input_modal}>
											<MyInput
												width="100%"
												backgroundColor="#ededed"
												className={styles.item__input_modal}
												type="text"
												name="name"
												icon={
													<>
														<FontAwesomeIcon icon={faUserCircle} />{' '}
													</>
												}
												placeholder="Ваше имя"
											/>
											<MyInput
												width="100%"
												name="email"
												backgroundColor="#ededed"
												className={styles.item__input_modal}
												type="email"
												icon={
													<>
														<FontAwesomeIcon icon={faEnvelope} />{' '}
													</>
												}
												placeholder="Ваш актуальный e-mail"
											/>
										</div>
										<div className={styles.container__btn_modal}>
											<div className={styles.btn_home_box_modal}>
												<Button
													type="submit"
													title="получить доступ"
													width="100%"
													height="70px"
													padd="0 75"
													// onClick={'#'}
												/>
											</div>

											<div className={styles.checkbox_license_modal}>
												<input type="checkbox" checked readOnly />
												<span className={styles.text_checkbox_modal}>
													Я согласен на обработку персональных данных и
													получение рекламных материалов, и я согласен с
													<Link
														className={styles.link_checkbox_modal}
														to="/polices"
													>
														публичной офертой
													</Link>
												</span>
											</div>
										</div>
									</>
								}
							/>
						</>
					}
				/>
				<Footer
					dark={dark}
					children={
						<>
							<ul className={styles.menu_footer}>
								<li className={styles.item_footer}>
									<Link className={styles.link} to="/polices">
										Политика конфиденциальности
									</Link>
								</li>
								<li className={styles.item_footer}>
									<Link className={styles.link} to="/agreement">
										Пользовательское соглашение
									</Link>
								</li>
							</ul>
						</>
					}
				/>
			</section>
		</Container>
	);
};

Home.propTypes = {};

export default Home;
