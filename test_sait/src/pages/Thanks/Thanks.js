import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import styles from './Thanks.module.scss';
import Container from '../../components/Containers/Container';
import Button from '../../components/Button/Button';

const Thanks = props => {
	const history = useHistory();
	const goToBack = () => {
		history.push('/');
	};
	return (
		<Container>
			<div className={styles.container__thanks}>
				<div className={styles.row__thanks}>
					<div className={styles.thanks}>
						<p className={styles.thanks_text}>
							В течении 24 часов на указанный email придет письмо с подробной
							инструкцией.
							<br />
							Если письмо не появилось в папке «Входящие», проверьте папку
							«Спам».
							<br />
							Если вы не получили письмо, пожалуйста, обратитесь в тех.
							поддержку. После получения письма:
							<br />
							1. Добавьте адрес отправителя в контакты
							<br />
							2. Если письмо попало в папку «Спам», выделите его и нажмите «Не
							спам», чтобы в дальнейшем гарантированно получать всю важную
							информацию по проекту «Общее дело».
							<br />
							Не забудьте нажать кнопку "Ок"
						</p>
					</div>
					<div className={styles.box__btn}>
						<Button
							className={styles.btn}
							backgroundColor="#12bd00"
							onClick={goToBack}
							title="На главную"
						/>
						{/* <div className={styles.btn}>
							<a href="/">На главную</a>
						</div> */}
					</div>
				</div>
			</div>
		</Container>
	);
};

Thanks.propTypes = {};

export default Thanks;
