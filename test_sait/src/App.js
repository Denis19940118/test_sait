import React from 'react';
import styles from './App.module.scss';
import AppRoute from './routes/AppRoute/AppRoute';

function App() {
	return (
		<div className={styles.App}>
			<AppRoute />
		</div>
	);
}

export default App;
